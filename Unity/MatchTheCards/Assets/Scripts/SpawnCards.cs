﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SpawnCards : MonoBehaviour {

    public GameObject[] cards = new GameObject[18];
    List<int> list = new List<int>();
    int number = 0;

    // Use this for initialization
    void Start()
    {
        PutCards();
    }

        void PutCards()
            { 
        do
        {
            int spawnCard = Random.Range(0, cards.Length);

            if (!list.Contains(spawnCard))
            {
                list.Add(spawnCard);
                Instantiate(cards[spawnCard], cards[number].transform.position, cards[number].transform.rotation);
                number++;
            }

        } while (number < cards.Length);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
